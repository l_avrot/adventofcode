Pour chaque ligne du document de réglages,
    # Attention: s'il n'y a qu'un seul chiffre sur toute la ligne, alors le
    # premier chiffre est aussi le dernier!
    On prend le premier chiffre de la ligne, on l'appelera "Dogo"
    On prend le dernier chiffre de la ligne. on l'appelera "Lala"
    On colle "Dogo" et "Lala" pour former un nombre qu'on appellera "unNom"
    On met "unNom" dans un tableau nommé "leRabeaut"

La première étoile est la somme de tous les nombres de "leRabeaut"

module main

import os { read_lines }
import flag
import encoding.utf8 { get_uchar }

// This should solve both parts of Day 2 of Advent of code 2022
fn main() {
	// Let's play with flags
	mut fp := flag.new_flag_parser(os.args)
	fp.application('2022/05')
	fp.limit_free_args(0, 0)!
	fp.skip_executable()
	example := fp.bool('example', 0, false, 'A boolean to use the example file instead of the complete input. --example will set it to true.')

	// Let's read the right input
	mut path := 'input.txt'
	if example {
		path = 'example.txt'
	}

	//Call part1
	line := part1(path)
	//Call part2
	part2(line)
}

// This should solve part 1 of Day 5 of Advent of code 2022
fn part1(path string) string {

	// Let's read the file
	mut lines := []string{}

	// Check if file exists
	if os.is_file(path) == true {
		lines << os.read_lines(path) or {panic('not read: $path')}
	}

	// I'd like to manipulate the rune datatype, so let's explore it for the first
	// part!
	mut marker := lines[0].substr(0,4).runes()

	mut i := 4
	// There's only 1 line in the input
	for {
		// No safety for this infinite loop! YOLO!!!
		// We'll just trust the elves to have the input right
		if is_marker_unique(marker) {
			// Let's write the result down
			println('PART1: here s the beginning of the marker ${i}')
			break
		}
		//Let's create the new marker
		// We remove the first element
		marker.delete(0)
		// We add the new element
		marker << lines[0].substr(i, i+1).runes()[0]
		i++
	}
	return lines[0]
}

fn is_marker_unique (marker []rune) bool {
	return (
		// Test if first rune is repeated
		marker[0] != marker[1] && marker[0] != marker[2] && marker[0] != marker[3] &&
		// Test if second rune is repeated
		marker[1] != marker[2] && marker[1] != marker[3] &&
		// Test if third rune is repeated
		marker[2] != marker[0] && marker[2] != marker[3]
	)
}
//
// This should solve part 2 of Day 5 of Advent of code 2022
fn part2(line string) {
	// let's stop complicate things by using runes and let's rely on a simple
	// string
	mut marker := line.substr(0,14)

	mut i := 14
	// There's only 1 line in the input
	for {
		// No safety for this infinite loop! YOLO!!!
		// We'll just trust the elves to have the input right
		if is_start_message(marker) {
			// Let's write the result down
			println('PART2: here s the beginning of the marker ${i}')
			break
		}
		//Let's create the new marker
		// We remove the first element
		marker = marker.substr(1, 14)
		// We add the new element
		marker = marker + line.substr(i, i+1)
		i++
	}
}

fn is_start_message(marker string) bool {
	mut is_unique := true
	for i := 0 ; i < marker.len ; i++ {
		if marker.count(marker.substr(i,i+1)) > 1 {
			is_unique = false
		}
	}
	return is_unique
}

module main

import os { read_lines }
import flag
import encoding.utf8 { get_uchar }

// This should solve both parts of Day 2 of Advent of code 2022
fn main() {
	// Let's play with flags
	mut fp := flag.new_flag_parser(os.args)
	fp.application('2022/03')
	fp.limit_free_args(0, 0)!
	fp.skip_executable()
	example := fp.bool('example', 0, false, 'A boolean to use the example file instead of the complete input. --example will set it to true.')

	// Let's read the right input
	mut path := 'input.txt'
	if example {
		path = 'example.txt'
	}

	//Call part1
	input := part1(path)
	//Call part2
	part2(input)
}

// This should solve part 1 of Day 3 of Advent of code 2022
fn part1(path string) []string {

	// Let's read the file
	mut lines := []string{}
	// We'll try to work with 
	mut elf_1_lo := 0
	mut elf_1_hi := 0
	mut elf_2_lo := 0
	mut elf_2_hi := 0
	mut useless := 0

	// Check if file exists
	if os.is_file(path) == true {
		lines << os.read_lines(path) or {panic('not read: $path')}
	}

	for line in lines {
		// Get bounds for both elves
		elf_1_lo=line.split('-')[0].int()
		elf_1_hi=line.split(',')[0].split('-')[1].int()
		elf_2_lo=line.split(',')[1].split('-')[0].int()
		elf_2_hi=line.split(',')[1].split('-')[1].int()

		// Is elf2 section contained by elf1 section?
		if (elf_1_lo <= elf_2_lo && elf_1_hi >= elf_2_hi) ||
			// Is elf1 section containd by elf2 section?
			 (elf_2_lo <= elf_1_lo && elf_2_hi >= elf_1_hi) {
			useless++
		}
	}
	println('PART1: There are ${useless} assignment pairs where one range fully contain the other')

	return lines
}

// This should solve part 2 of Day 3 of Advent of code 2022
fn part2(lines []string) {
	// We'll try to work with 
	mut elf_1_lo := 0
	mut elf_1_hi := 0
	mut elf_2_lo := 0
	mut elf_2_hi := 0
	mut useless := 0

	for line in lines {
		// Get bounds for both elves
		elf_1_lo=line.split('-')[0].int()
		elf_1_hi=line.split(',')[0].split('-')[1].int()
		elf_2_lo=line.split(',')[1].split('-')[0].int()
		elf_2_hi=line.split(',')[1].split('-')[1].int()

		if (elf_1_lo <= elf_2_hi && elf_1_lo >= elf_2_lo) ||
			 (elf_2_lo <= elf_1_hi && elf_2_lo >= elf_1_lo) {
			useless++
		}
	}
	println('PART2: There are ${useless} assignment pairs where one range fully contain the other')
}
//

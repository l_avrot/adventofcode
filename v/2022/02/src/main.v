module main

import os { read_lines }
import flag

// This should solve both parts of Day 2 of Advent of code 2022
fn main() {
	// Let's play with flags
	mut fp := flag.new_flag_parser(os.args)
	fp.application('2022/02')
	fp.limit_free_args(0, 0)!
	fp.skip_executable()
	example := fp.bool('example', 0, false, 'A boolean to use the example file instead of the complete input. --example will set it to true.')

	// Let's read the right input
	mut path := 'input.txt'
	if example {
		path = 'example.txt'
	}

	//Call part1
	input := part1(path)
	//Call part2
	part2(input)
}

// This should solve part 1 of Day 1 of Advent of code 2022
fn part1(path string) []string{
	
	// Let's read the file
	mut score := 0
	mut lines := []string{}

	// Let's create a map to decode the number of points of each round
	result := {
		// A for Rock, B for Paper, and C for Scissors
		// X for Rock, Y for Paper, and Z for Scissors
		// 1 for Rock, 2 for Paper, and 3 for Scissors
		// 0 if you lost, 3 if the round was a draw, and 6 if you won
		'A X': 4 // 1+3
		'A Y': 8 // 2+6
		'A Z': 3 // 3+0
		'B X': 1 // 1+0
		'B Y': 5 // 2+3
		'B Z': 9 // 3+6
		'C X': 7 // 1+6
		'C Y': 2 // 2+0
		'C Z': 6 // 3+3
	}

	// Check if file exists
	if os.is_file(path) == true {
		lines << os.read_lines(path) or {panic('not read: $path')}
	}

	for line in lines {
		score = score + result[line]
	}
	println('PART1: You should get a score of ${score}')

	return lines
}

// This should solve part 2 of Day 1 of Advent of code 2022
fn part2(lines []string) {
	mut score := 0

	// Let's create a map to decode the number of points of each round
	result := {
		// A for Rock, B for Paper, and C for Scissors
		// X means you need to lose, Y means you need to end the round in a draw,
		// and Z means you need to win
		// 1 for Rock, 2 for Paper, and 3 for Scissors
		// 0 if you lost, 3 if the round was a draw, and 6 if you won
		'A X': 3 // Scissors + lose -> 3+0
		'A Y': 4 // Rock + Draw -> 1+3
		'A Z': 8 // Paper + Win -> 2+6
		'B X': 1 // Rock + lose -> 1+0
		'B Y': 5 // Paper + Draw -> 2+3
		'B Z': 9 // Scissors + Win -> 3+6
		'C X': 2 // Paper + lose -> 2+0
		'C Y': 6 // Scissors + Draw -> 3+3
		'C Z': 7 // Rock + Win -> 1+6
	}

	for line in lines {
		score = score + result[line]
	}

	println('PART2: You should get a score of ${score}')
}


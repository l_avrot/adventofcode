module main

import os { read_lines }
import flag
import encoding.utf8 { get_uchar }

// This should solve both parts of Day 2 of Advent of code 2022
fn main() {
	// Let's play with flags
	mut fp := flag.new_flag_parser(os.args)
	fp.application('2022/05')
	fp.limit_free_args(0, 0)!
	fp.skip_executable()
	example := fp.bool('example', 0, false, 'A boolean to use the example file instead of the complete input. --example will set it to true.')

	// Let's read the right input
	mut path := 'input.txt'
	if example {
		path = 'example.txt'
	}

	//Call part1
	crates_input,instructions,columns,max_len := part1(path)
	//Call part2
	part2(crates_input,instructions,columns,max_len)
}

// This should solve part 1 of Day 5 of Advent of code 2022
fn part1(path string) ([]string,[][]int, int, int) {

	// Let's read the file
	mut lines := []string{}
	// Let's first create the matrix
	mut crates_input := []string{}
	mut instructions := [][]int{}
	mut is_crates_init := true
	mut max_len := 0
	mut columns := 0

	// Check if file exists
	if os.is_file(path) == true {
		lines << os.read_lines(path) or {panic('not read: $path')}
	}

	for line in lines {
		//If the line is empty: then we will begin instructions
		if line.len == 0 {
			is_crates_init = false
			// Let's memorize the number of columns
			columns = crates_input[crates_input.len-1].substr(
				crates_input[crates_input.len-1].len-1,
				crates_input[crates_input.len-1].len).int()
				
			// Let's remove the last line from crates_input as it's just the column
			// numbers
			crates_input.delete_last()
		}

		if is_crates_init {
			crates_input << line
			//Let's find the biggest line
			if max_len < line.len {
				max_len = line.len
			}
		} else if line.len > 0 {
			// We need to decode the instructions
			// Here's how the instructions look like:
			// Move x from y to z
			mut instr := []int{}
			i := line.index(' from') or { panic(err) }
			instr << line.substr(5,i).int()
			j := line.index(' to') or { panic(err) }
			instr << line.substr(i+6, j).int()
			instr << line.substr(j+4, line.len).int()
			instructions << instr
		}
	}	

	mut crates := order(crates_input, columns, max_len)

	// Let's move those crates!
	for instr in instructions {
		for j := 1; j <= instr[0]; j++ {
			// Let's make sure it's doable
			if crates[instr[1]-1].len > 0 {
				// Let's add the crate to the new column
				crates[instr[2]-1] = crates[instr[1]-1].substr(0,1) + crates[instr[2]-1]
				// Let's remove the crate from the former column
				crates[instr[1]-1] = crates[instr[1]-1].substr(1, crates[instr[1]-1].len)
			}
		}
	}

	// Get the available crates
	mut available := ''
	for item in crates {
		available = available + item.substr(0,1)
	}
	println('PART1: here are all the available crates ${available}')

	return crates_input, instructions, columns, max_len
}

// This should solve part 2 of Day 5 of Advent of code 2022
fn part2(crates_input []string, instructions [][]int, columns int, max_len int) {
	mut crates := order(crates_input, columns, max_len)
	// Let's move those crates!
	for instr in instructions {
			// Let's add the crates to the new column
			crates[instr[2]-1] = crates[instr[1]-1].substr(0,instr[0]) + crates[instr[2]-1]
			// Let's remove the crate from the former column
			crates[instr[1]-1] = crates[instr[1]-1].substr(instr[0], crates[instr[1]-1].len)
	}

	// Get the available crates
	mut available := ''
	for item in crates {
		available = available + item.substr(0,1)
	}
	println('PART2: here are all the available crates ${available}')
}

fn order(crates_input []string, columns int, max_len int) []string{
	// We will represent the crates in lines (with strings) as it will be easier
	// than a multidimensional array (at least, for me)
	// It means we will implement a pivot table!!!! Yay!!!
	mut crates := []string{len: columns}

	// Let's put available crates as the first one of each array item
	for item in crates_input {
		for i := 0 ; 2+4*i < max_len && i < columns ; i++ {
			if item.len > 2+4*i  && item.substr(1+4*i,2+4*i) != ' ' {
				crates[i] = crates[i] + item.substr(1+4*i,2+4*i)
			}
		}
	}

	return crates
}

module main

import os { read_lines }
import flag

// This should solve both parts of Day 1 of Advent of code 2022
fn main() {
	// Let's play with flags
	mut fp := flag.new_flag_parser(os.args)
	fp.application('2022/01')
	fp.limit_free_args(0, 0)!
	fp.skip_executable()
	example := fp.bool('example', 0, false, 'A boolean to use the example file instead of the complete input. --example will set it to true.')

	println('Titianette 0')
	// Let's read the right input
	mut path := 'input.txt'
	if example {
		path = 'example.txt'
	}

	//Call part1
	elves := part1(path)
	//Call part2
	part2(elves)
}

// This should solve part 1 of Day 1 of Advent of code 2022
fn part1(path string) []int {
	
	// Let's read the file
	mut elves := [0]
	mut elf_with_most_calories := 0
	mut lines := []string{}

	// Check if file exists
	if os.is_file(path) == true {
		lines << os.read_lines(path) or {panic('not read: $path')}
	}

	for line in lines {
		// If the line is empty, then it's a brand new elf!!!!
		if line.len == 0 {

			// Let's get the elf with most calories
      if elf_with_most_calories < elves[elves.len-1] {
				elf_with_most_calories = elves[elves.len-1]

			}

			// Next Elf
			elves << 0

		} else {
			// Let's add those calories to the ones that elves was already carrying
			elves[elves.len-1] += line.int()
		}
	}

	println('The total calories the elf carrying the most calories is ${elf_with_most_calories}')

	return elves
}

// This should solve part 2 of Day 1 of Advent of code 2022
fn part2(elves []int) {
	mut calories := elves.clone()
	calories.sort(b < a)

	println('The total calories the three elves carrying the most calories is ${calories[0] + calories[1] + calories[2]}')
}


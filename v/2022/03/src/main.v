module main

import os { read_lines }
import flag
import encoding.utf8 { get_uchar }

// This should solve both parts of Day 2 of Advent of code 2022
fn main() {
	// Let's play with flags
	mut fp := flag.new_flag_parser(os.args)
	fp.application('2022/03')
	fp.limit_free_args(0, 0)!
	fp.skip_executable()
	example := fp.bool('example', 0, false, 'A boolean to use the example file instead of the complete input. --example will set it to true.')

	// Let's read the right input
	mut path := 'input.txt'
	if example {
		path = 'example.txt'
	}

	//Call part1
	input := part1(path)
	//Call part2
	part2(input)
}

// This should solve part 1 of Day 3 of Advent of code 2022
fn part1(path string) []string {

	// Let's read the file
	mut score := 0
	mut lines := []string{}
	mut compartiment1 := []string{}
	mut compartiment2 := ''

	// Check if file exists
	if os.is_file(path) == true {
		lines << os.read_lines(path) or {panic('not read: $path')}
	}

	for line in lines {
		// Let's get both compartiments first.
		compartiment1 = line.substr(0,line.len/2).split('')
		compartiment2 = line.substr((line.len/2), line.len)
		// Let's find the common item
		for item in compartiment1 {
			if compartiment2.contains(item) {
				// Let's decode that into a number
				// Let's sum
				score = score + decode_value(item)
				// We've accomplished the mission, let's go to the elf's rucksack
				break
			}
		}
	}
	println('PART1: You should get a score of ${score}')

	return lines
}

fn decode_value(item string) int {
	mut score := 0

	if item.is_lower() {
		score = get_uchar(item,0) - 96
	} else {
		score = get_uchar(item,0) - 38
	}

	return score
}

// This should solve part 2 of Day 3 of Advent of code 2022
fn part2(lines []string) {
	mut score := 0
	mut first_elf := []string{}
	mut second_elf := ''
	mut third_elf := ''

	for i in 0 .. lines.len {
		// let's get the elf in the right place
		if i%3 == 0 {
			first_elf = lines[i].split('')
		} else if i%3 == 1 {
			second_elf = lines[i]
		} else {
			third_elf = lines[i]

			// Now that we have all the rucksacks, let's find the badge!
			for item in first_elf {
				if second_elf.contains(item) && third_elf.contains(item) {
					// Let's decode that into a number
					// Let's sum
					score = score + decode_value(item)
					// We've accomplished the mission, let's go to the elf's rucksack
					break
				}
			}
		}
	}

	println('PART2: You should get a score of ${score}')
}
//
